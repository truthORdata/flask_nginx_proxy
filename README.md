build base
build flask from base (while in flask_compose directory), call it flask:mini

push flask image to minishift

change the nip.io IP address to your minishift IP

apply frontend and backend yamls to minishift

now you can talk to frontend container from terminal, which communicates with internal backend container: curl http://MINISHIFT_ROUTE/view

can also test frontend only via curl http://MINISHIFT_ROUTE/test