from flask import Flask, request, abort
import json

app = Flask(__name__)


data = [{'type': 'carrot', 'wt': 5}]
host = "0.0.0.0"
port = "7001"


@app.route('/append', methods=['POST'])
def consume():
    if request.method == 'POST':
        post_data = request.json
        data.append(post_data)
        print(data)
        return '{"ret": "data added!"}', 200
    else:
        abort(400)


@app.route('/reveal', methods=['GET'])
def show():
    if request.method == 'GET':
        print('GET request received!')
        d = json.dumps(data)
        return d, 200
    else:
        abort(400)


@app.route('/test')
def test():
    return "IT WORKS"


if __name__ == '__main__':
    app.run(host=host, port=port)
